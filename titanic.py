# This Python 3 environment comes with many helpful analytics libraries installed
# It is defined by the kaggle/python docker image: https://github.com/kaggle/docker-python
# For example, here's several helpful packages to load in 

import numpy as np # linear algebra
import pandas as pd # data processing, CSV file I/O (e.g. pd.read_csv)

# read files
train_data = pd.read_csv("../input/train.csv")
test_data  = pd.read_csv("../input/test.csv")

# check for missing target values in training data
if train_data.Survived.isnull().sum() == 0:
    print("No missing values for classification variable")

# check which variables we have in training data
print("Training data summary")
print(train_data.info())
print("Testing data summary")
print(test_data.info())

# training data missing: Age, Cabin, Embarked
# testing data missing:  Age, Cabin, Fare

# dealing with mising data
totalTrain = train_data.shape[0]
totalTest  = test_data.shape[0]

# training
print("Missing Age for train: {missing} / {total}".format(missing=train_data.Age.isnull().sum(),total=totalTrain))
print("Missing Cabin for train: {missing} / {total}".format(missing=train_data.Cabin.isnull().sum(),total=totalTrain))
print("Missing Embarked for train: {missing} / {total}".format(missing=train_data.Embarked.isnull().sum(),total=totalTrain))

# testing
print("Missing Age for test: {missing} / {total}".format(missing=test_data.Age.isnull().sum(),total=totalTest))
print("Missing Cabin for test: {missing} / {total}".format(missing=test_data.Cabin.isnull().sum(),total=totalTest))
print("Missing Fare for test: {missing} / {total}".format(missing=test_data.Fare.isnull().sum(),total=totalTest))

# missing Embarked, Fare in training dataset is neglible
# fill Embarekd with mode
train_data["Embarked"] = train_data["Embarked"].fillna("S")
# fill Fare with mean
train_data["Fare"] = train_data["Fare"].fillna(train_data.Fare.mean())

# missing age is large: 117/891 for training and 86/418 for testing
print("age for training: ")
print(train_data.Age.describe())
print("age for testing: ")
print(test_data.Age.describe())

#TODO:
# missing Age and cabin
# check variable correlations (drop some which are too correlated?)
# drop variables with no discrimination (passenger ID?)